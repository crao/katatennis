package org.kata.tennis.services;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.kata.tennis.model.GameScore;
import org.kata.tennis.model.ScoreEnum;

public class ScoringServiceTest {

	@Test
	public void should_playerOne_get_first_point() {
		//GIVEN
		ScoringService scoreService = new ScoringServiceImpl();
		boolean playerOneWinPoint = true;

		// WHEN
		GameScore gameScore = scoreService.determineScore(playerOneWinPoint);

		// THEN
		assertThat(gameScore.getPlayerOneScore()).isEqualTo(ScoreEnum.FIFTEEN);
		assertThat(gameScore.getPlayerTwoScore()).isEqualTo(ScoreEnum.LOVE);
	}

	@Test
	public void should_playerTwo_get_first_point() {
		//GIVEN
		ScoringService scoreService = new ScoringServiceImpl();
		boolean playerOneWinPoint = false;

		// WHEN
		GameScore gameScore = scoreService.determineScore(playerOneWinPoint);

		// THEN
		assertThat(gameScore.getPlayerOneScore()).isEqualTo(ScoreEnum.LOVE);
		assertThat(gameScore.getPlayerTwoScore()).isEqualTo(ScoreEnum.FIFTEEN);
	}

	@Test
	public void should_playerOne_get_thirty_after_fifteen() {
		//GIVEN
		ScoringService scoreService = new ScoringServiceImpl();
		boolean playerOneWinPoint = true;
		scoreService.determineScore(playerOneWinPoint);

		// WHEN
		GameScore gameScore = scoreService.determineScore(playerOneWinPoint);

		// THEN
		assertThat(gameScore.getPlayerOneScore()).isEqualTo(ScoreEnum.THIRTY);
		assertThat(gameScore.getPlayerTwoScore()).isEqualTo(ScoreEnum.LOVE);
	}

	@Test
	public void should_playerOne_get_forty_after_thirty() {
		//GIVEN
		ScoringService scoreService = new ScoringServiceImpl();
		boolean playerOneWinPoint = true;
		scoreService.determineScore(playerOneWinPoint);
		scoreService.determineScore(playerOneWinPoint);

		// WHEN
		GameScore gameScore = scoreService.determineScore(playerOneWinPoint);

		// THEN
		assertThat(gameScore.getPlayerOneScore()).isEqualTo(ScoreEnum.FORTY);
		assertThat(gameScore.getPlayerTwoScore()).isEqualTo(ScoreEnum.LOVE);
	}

	@Test
	public void should_playerOne_and_playerTwo_get_deuce() {
		//GIVEN
		ScoringService scoreService = new ScoringServiceImpl();
		boolean playerOneWinPoint = true;
		boolean playerTwoLoosePoint = false;
		scoreService.determineScore(playerOneWinPoint);
		scoreService.determineScore(playerOneWinPoint);
		scoreService.determineScore(playerOneWinPoint);

		scoreService.determineScore(playerTwoLoosePoint);
		scoreService.determineScore(playerTwoLoosePoint);

		// WHEN
		GameScore gameScore = scoreService.determineScore(playerTwoLoosePoint);

		// THEN
		assertThat(gameScore.getPlayerOneScore()).isEqualTo(ScoreEnum.DEUCE);
		assertThat(gameScore.getPlayerTwoScore()).isEqualTo(ScoreEnum.DEUCE);
	}

	@Test
	public void should_playerOne_get_advantage_after_deuce() {
		//GIVEN
		ScoringService scoreService = new ScoringServiceImpl();
		boolean playerOneWinPoint = true;
		boolean playerTwoLoosePoint = false;
		scoreService.determineScore(playerOneWinPoint);
		scoreService.determineScore(playerOneWinPoint);
		scoreService.determineScore(playerOneWinPoint);

		scoreService.determineScore(playerTwoLoosePoint);
		scoreService.determineScore(playerTwoLoosePoint);
		scoreService.determineScore(playerTwoLoosePoint);

		// WHEN
		GameScore gameScore = scoreService.determineScore(playerOneWinPoint);

		// THEN
		assertThat(gameScore.getPlayerOneScore()).isEqualTo(ScoreEnum.ADVANTAGE);
		assertThat(gameScore.getPlayerTwoScore()).isEqualTo(ScoreEnum.FORTY);
	}

	@Test
	public void should_playerOne_win_after_advantage() {
		//GIVEN
		ScoringService scoreService = new ScoringServiceImpl();
		boolean playerOneWinPoint = true;
		boolean playerTwoLoosePoint = false;
		scoreService.determineScore(playerOneWinPoint);
		scoreService.determineScore(playerOneWinPoint);
		scoreService.determineScore(playerOneWinPoint);

		scoreService.determineScore(playerTwoLoosePoint);
		scoreService.determineScore(playerTwoLoosePoint);
		scoreService.determineScore(playerTwoLoosePoint);

		scoreService.determineScore(playerOneWinPoint);

		// WHEN
		GameScore gameScore = scoreService.determineScore(playerOneWinPoint);

		// THEN
		assertThat(gameScore.getPlayerOneScore()).isEqualTo(ScoreEnum.WIN);
		assertThat(gameScore.getPlayerTwoScore()).isEqualTo(ScoreEnum.FORTY);
	}


	@Test
	public void should_playerOne_win_after_forty() {
		//GIVEN
		ScoringService scoreService = new ScoringServiceImpl();
		boolean playerOneWinPoint = true;
		scoreService.determineScore(playerOneWinPoint);
		scoreService.determineScore(playerOneWinPoint);
		scoreService.determineScore(playerOneWinPoint);

		// WHEN
		GameScore gameScore = scoreService.determineScore(playerOneWinPoint);

		// THEN
		assertThat(gameScore.getPlayerOneScore()).isEqualTo(ScoreEnum.WIN);
		assertThat(gameScore.getPlayerTwoScore()).isEqualTo(ScoreEnum.LOVE);
	}
}
