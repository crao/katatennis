package org.kata.tennis;

import org.kata.tennis.services.ScoringService;
import org.kata.tennis.services.ScoringServiceImpl;

public class Main {

    public static void main(String[] args) {
	// write your code here
        ScoringService scoringService = new ScoringServiceImpl();

        boolean playOneWinPoint = true;
        scoringService.determineScore(playOneWinPoint);
    }
}
