package org.kata.tennis.model;

public class GameScore {

	private ScoreEnum playerOneScore;

	private ScoreEnum playerTwoScore;

	public GameScore(ScoreEnum playerOneScore, ScoreEnum playerTwoScore) {
		this.playerOneScore = playerOneScore;
		this.playerTwoScore = playerTwoScore;
	}

	public ScoreEnum getPlayerOneScore() { return playerOneScore; }

	public ScoreEnum getPlayerTwoScore() {
		return playerTwoScore;
	}

	public void updateScoreWhenPlayerOneWin() {
		playerOneScore = playerOneScore.determineNextScore(playerTwoScore);
		playerTwoScore = playerTwoScore.determineLoosingScore(playerOneScore);
	}

	public void updateScoreWhenPlayerTwoWin() {
		playerTwoScore = playerTwoScore.determineNextScore(playerOneScore);
		playerOneScore = playerOneScore.determineLoosingScore(playerTwoScore);
	}
}
