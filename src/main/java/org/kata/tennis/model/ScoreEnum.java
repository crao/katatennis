package org.kata.tennis.model;

public enum ScoreEnum {

	LOVE() {
		@Override
		public ScoreEnum determineNextScore(ScoreEnum opponentScore) {
			return getNextOrdinalValue();
		}

		@Override
		public ScoreEnum determineLoosingScore(ScoreEnum opponentScore) {
			return this;
		}
	},
	FIFTEEN() {
		@Override
		public ScoreEnum determineNextScore(ScoreEnum opponentScore) {
			return getNextOrdinalValue();
		}

		@Override
		public ScoreEnum determineLoosingScore(ScoreEnum opponentScore) {
			return this;
		}
	},
	THIRTY() {
		@Override
		public ScoreEnum determineNextScore(ScoreEnum opponentScore) {
			return opponentScore.equals(ScoreEnum.FORTY) ? ScoreEnum.DEUCE : ScoreEnum.FORTY;
		}

		@Override
		public ScoreEnum determineLoosingScore(ScoreEnum opponentScore) {
			return this;
		}
	},
	FORTY() {
		@Override
		public ScoreEnum determineNextScore(ScoreEnum opponentScore) {
			return ScoreEnum.WIN;
		}

		@Override
		public ScoreEnum determineLoosingScore(ScoreEnum opponentScore) {
			return opponentScore.equals(ScoreEnum.DEUCE) ? ScoreEnum.DEUCE : ScoreEnum.FORTY;
		}
	},
	DEUCE() {
		@Override
		public ScoreEnum determineNextScore(ScoreEnum opponentScore) {
			return getNextOrdinalValue();
		}

		@Override
		public ScoreEnum determineLoosingScore(ScoreEnum opponentScore) {
			return ScoreEnum.FORTY;
		}
	},
	ADVANTAGE() {
		@Override
		public ScoreEnum determineNextScore(ScoreEnum opponentScore) {
			return getNextOrdinalValue();
		}

		@Override
		public ScoreEnum determineLoosingScore(ScoreEnum opponentScore) {
			return ScoreEnum.FORTY;
		}
	},
	WIN() {
		@Override
		public ScoreEnum determineNextScore(ScoreEnum opponentScore) {
			return this;
		}

		@Override
		public ScoreEnum determineLoosingScore(ScoreEnum opponentScore) {
			return this;
		}
	};

	public ScoreEnum getNextOrdinalValue() {
		return this.ordinal() < ScoreEnum.values().length - 1 ? ScoreEnum.values()[this.ordinal() +1] : WIN;
	}

	public abstract ScoreEnum determineNextScore(ScoreEnum opponentScore);

	public abstract ScoreEnum determineLoosingScore(ScoreEnum opponentScore);
}
