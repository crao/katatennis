package org.kata.tennis.services;

import org.kata.tennis.model.GameScore;
import org.kata.tennis.model.ScoreEnum;

public class ScoringServiceImpl implements ScoringService {

	public GameScore score;

	public ScoringServiceImpl() {
		this.score = new GameScore(ScoreEnum.LOVE, ScoreEnum.LOVE);
	}

	public GameScore determineScore(boolean playerOneWinPoint) {

		if (playerOneWinPoint) {
			score.updateScoreWhenPlayerOneWin();
		} else {
			score.updateScoreWhenPlayerTwoWin();
		}
		return score;
	}
}
