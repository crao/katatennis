package org.kata.tennis.services;

import org.kata.tennis.model.GameScore;

public interface ScoringService {

	GameScore determineScore(boolean playerOneWinPoint);

}
